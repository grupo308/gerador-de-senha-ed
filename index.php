<!DOCTYPE html>

<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gerador de Senhha</title>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;400;700&display=swap');
    body {
        font-family: 'Poppins', sans-serif;
    }
    #geral{
        width:400px;
        border:1px solid #ccc;
        padding:20px;
        margin: 20px auto;
        border-radius: 5px;
        box-shadow: 0px 5px 30px #ccc;


 }     

 </style>

</head>
<body>
    <div id="geral">
        <header>
            <h1>Gerador de Senha</h1>
        </header>
        <main>
            <form action="gerar.php" method="get">
                <p>
                    <label>Limite de Senha:</label>
                    <input type="text" name="limite">
                </p>
                <p>
                    <button type="submit">Gerar Senha</button>
                </p>
            </form>
        </main>
        <footer>
            <p>Copyright - 2022 - Desenvolvido por Matheus</p>
        </footer>
    </div>
</body>
</html>